import { useEffect, useState } from 'react';

function AttendeesList() {
    const [attendees, setAttendees] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8001/api/attendees/');

        if (response.ok) {
            const data = await response.json();
            setAttendees(data.attendees)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Attendees List</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Conferences</th>
                            </tr>
                        </thead>
                        <tbody>
                            {attendees.map(attendee => {
                                return (
                                    <tr key={attendee.href}>
                                        <td>{ attendee.name }</td>
                                        <td>{ attendee.conference }</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default AttendeesList;
