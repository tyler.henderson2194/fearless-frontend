import React, { useState, useEffect } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [presentations, setPresentations] = useState('');
    const [attendees, setAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handlePresentationChange = (event) => {
        const value = event.target.value;
        setPresentations(value);
    }

    const handleAttendeeChange = (event) => {
        const value = event.target.value;
        setAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = attendees;
        data.location = location;


        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setPresentations('');
            setAttendees('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartChange} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control" value={starts} />
                            <label htmlFor="room_count">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndChange} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control" value={ends} />
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <label htmlFor="description">Description</label>
                        <div className="input-group mb-3">
                            <textarea onChange={handleDescriptionChange} className="form-control" id="description" aria-label="With textarea" value={description}></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePresentationChange} placeholder="maximum presentations" required type="text" name="max_presentations" id="presentations" className="form-control" value={presentations}/>
                            <label htmlFor="presentation">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAttendeeChange} placeholder="maximum attendees" required type="text" name="max_attendees" id="attendees" className="form-control" value={attendees}/>
                            <label htmlFor="attendees">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                    <option key={location.href} value={location.id}>
                                        {location.name}
                                    </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
            </div>
        </div>
    </div>
    );
}

export default ConferenceForm;
